/*
 * when looking at the motor drive shaft
 * sensorA is on the left (9 oclock) (pin 9)
 * sensorB is on the right (3 oclock) (pin 10)
 * 
 * cat5 port #2 -> arduino pin 9
 * cat5 port #3 -> arduino pin 10
 * cat5 port #5 -> ground (2nd from bottom right)
 * cat5 port #4 -> 3.3v (bottom right)
 * 
 * relay 1 -> pin 2
 * relay 3 -> pin 3
 * relay 4 -> pin 4
 */

const char RELAY_OFF = 1;
const char RELAY_TOWARDS_A = 2;
const char RELAY_TOWARDS_B = 3;
const char POS_A = 4;
const char POS_B = 5;

const int POWER_RELAY = 2;
const int UP_RELAY = 3;
const int DOWN_RELAY = 4;

const int led = 13;
const int sensorA = 9;
const int sensorB = 10;


// DEBUGGING
//const unsigned long MOTOR_TIMEOUT = 3000;
//const unsigned long TIME_BETWEEN = 8000;
//const unsigned long MIN_MOTOR_RUN = 200;

// REAL
const unsigned long MOTOR_TIMEOUT = 800;
const unsigned long TIME_BETWEEN = 24000;
const unsigned long MIN_MOTOR_RUN = 50;



bool startupMode = true;
bool ledOn = false;
int relayState;
int positionState;
unsigned long waitUntil = 0;
unsigned long motorStartTime = 0;


void off() {
  if (relayState == RELAY_OFF) return;
  relayState = RELAY_OFF;
  motorStartTime = 0;
  digitalWrite(POWER_RELAY, HIGH);
  digitalWrite(UP_RELAY, HIGH);
  digitalWrite(DOWN_RELAY, HIGH);  
  Serial.println("off");
}

void towardsA() {
  if (relayState == RELAY_TOWARDS_A) return;
  relayState = RELAY_TOWARDS_A;
  motorStartTime = millis();
  digitalWrite(POWER_RELAY, LOW);
  digitalWrite(UP_RELAY, HIGH);
  digitalWrite(DOWN_RELAY, LOW);
  Serial.println("towardsA");
}

void towardsB() {
  if (relayState == RELAY_TOWARDS_B) return;
  relayState = RELAY_TOWARDS_B;
  motorStartTime = millis();
  digitalWrite(POWER_RELAY, LOW);
  digitalWrite(UP_RELAY, LOW);
  digitalWrite(DOWN_RELAY, HIGH);
  Serial.println("towardsB");
}

void setup() {
  pinMode(POWER_RELAY, OUTPUT);
  pinMode(UP_RELAY, OUTPUT);
  pinMode(DOWN_RELAY, OUTPUT);

  pinMode(led, OUTPUT);
  pinMode(sensorA, INPUT_PULLUP);
  pinMode(sensorB, INPUT_PULLUP);

  startupMode = true;
  Serial.begin(9600);
  Serial.println("hello");


  off();
  delay(500);
  off();

  
  waitUntil = millis() + 3000;

}

// I have to read LOW 3 times in a row to be true
// LOW means sensor active
bool safeRead(int pin) {
  for (int i = 0; i < 4; i++) {
    if (digitalRead(pin) == HIGH) {
      return false;
    }
  }
  return true;
}

void halt() {
  off();
  if (positionState == POS_A) {
    Serial.println("position state is POS_A");
  } else if (positionState == POS_B) {
    Serial.println("position state is POS_B");
  } else {
    Serial.println("Unknown positionState");
    Serial.println(positionState);
    Serial.println(startupMode);
  }

  while(true) {
    digitalWrite(led, HIGH);
    delay(2000);
    digitalWrite(led, LOW);
    delay(2000);
  }
}

void loop() {

  bool atSensorA = digitalRead(sensorA) == LOW;
  bool atSensorB = digitalRead(sensorB) == LOW;

  if ((atSensorA || atSensorB) && !ledOn) {
    digitalWrite(led, HIGH);
    ledOn = true;
  } else if (ledOn) {
    digitalWrite(led, LOW);
    ledOn = false;
  }

  unsigned long now = millis();
  unsigned long motorRunTime = 0;
  bool isMotorOn = relayState != RELAY_OFF;
  if (isMotorOn) {
    motorRunTime = now - motorStartTime;
  }
  bool isValidStop = startupMode || motorRunTime > MIN_MOTOR_RUN;

  //
  // Check for stop conditions
  //
  if (
    atSensorA &&
    positionState != POS_A &&
    relayState == RELAY_TOWARDS_A &&
    isValidStop
  ) {
    off();
    positionState = POS_A;
    startupMode = false;
    Serial.println("sensorA");
    if (!safeRead(sensorA)) {
      Serial.println("sensorA, but safeRead(a) == false");
    }
    waitUntil = now + TIME_BETWEEN;
  }

  if (
    atSensorB &&
    positionState != POS_B &&
    relayState == RELAY_TOWARDS_B &&
    isValidStop
  ) {
    off();
    positionState = POS_B;
    startupMode = false;
    Serial.println("sensorB");
    if (!safeRead(sensorB)) {
      Serial.println("sensorB, but safeRead(b) == false");
    }
    waitUntil = now + TIME_BETWEEN;
  }


  //
  // Check for motor timeout
  // and fast-blink abort
  //
  if (motorStartTime > 0 && now - motorStartTime > MOTOR_TIMEOUT) {
    Serial.println("HALT: motor timeout");
    off();
    while(true) {
      digitalWrite(led, HIGH);
      delay(500);
      digitalWrite(led, LOW);
      delay(500);
    }
  }



  //
  // Check for motor start.
  // When now > waitUntil start motor
  //
  if (waitUntil > 0 && now > waitUntil) {
    waitUntil = 0;
    if (startupMode == 1) {
      if (safeRead(sensorB)) {
        Serial.println("startupMode towardsA");
        towardsA();
      } else {
        Serial.println("startupMode towardsB");
        towardsB();
      }
    }
    else if (positionState == POS_B) {
      if (!safeRead(sensorB)) {
        Serial.println("WARN: positionState is B, but safeRead(b) is false... proceeding.");
      }
      towardsA();
    } else if (positionState == POS_A) {
      if (!safeRead(sensorA)) {
        Serial.println("WARN: positionState is A, but safeRead(a) is false... proceeding.");
      }
      towardsB();
    } else {
      Serial.println("HALT: no safe read");
      halt();
    }
  }

}
